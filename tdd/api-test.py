import unittest
import requests
import secret


url = 'https://reqres.in/api/login'

def make_request(url, user, passwd):
    r = requests.post(url, {'email': user, 'password': passwd})
    return r.status_code


class TestAPIAuth(unittest.TestCase):
    def test_login_with_real_user_data(self):
        st = make_request(url, secret.user, secret.passwd)
        self.assertEqual(st, 200)

    def test_login_with_fake_user_data(self):
        st = make_request(url, 'fakeuser100', 'password123')
        # api works with all sorts of data
        # so we assume that response is 401
        self.assertEqual(st, 200)

    def test_login_without_username(self):
        st = make_request(url, '', 'password123')
        self.assertEqual(st, 400)

    def test_login_without_password(self):
        st = make_request(url, secret.user, '')
        self.assertEqual(st, 400)

    def test_login_without_data(self):
        st = make_request(url, '', '')
        self.assertEqual(st, 400)



if __name__ == '__main__':
    unittest.main()
