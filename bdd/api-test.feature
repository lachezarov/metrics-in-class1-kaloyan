Feature: test API auth with different types of data

    Scenario: using real user data
        Given we try to login as a real user
        when we provide real user data
        then we should receive success status "200"

    Scenario: using fake user data
        Given we try to login as a non-existing user
        when we provide fake user data
        then we should receive error status "200"

    Scenario: without username
        Given we try to login without a username
        when we don't provide a username
        then we should receive error2 status "400"

    Scenario: without password
        Given we try to login without a password
        when we don't provide a password
        then we should receive error3 status "400"

    Scenario: without any data
        Given we try to login without data
        when we don't provide a username or password
        then we should receive error4 status "400"
