import requests
from behave import *

@given('we try to login as a real user')
def step_impl(context):
    pass

@when('we provide real user data')
def step_impl(context):
    res = requests.post('https://reqres.in/api/login', {'email': 'real_user', 'password': 'real_password'})
    context.status = str(res.status_code)

@then('we should receive success status "{text}"')
def step_impl(context, text):
    print(type(text), type(context.status))
    assert text == context.status
