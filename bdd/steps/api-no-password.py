import requests
from behave import *


@given('we try to login without a password')
def step_impl(context):
    pass

@when('we don\'t provide a password')
def step_impl(context):
    res = requests.post('https://reqres.in/api/login', {'email': 'real_user', 'password': ''})
    context.status = str(res.status_code)

@then('we should receive error3 status "{text}"')
def step_impl(context, text):
    assert text == context.status
