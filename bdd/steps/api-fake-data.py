import requests
from behave import *


@given('we try to login as a non-existing user')
def step_impl(context):
    pass

@when('we provide fake user data')
def step_impl(context):
    res = requests.post('https://reqres.in/api/login', {'email': 'fakeuser100', 'password': 'password123'})
    context.status = str(res.status_code)

@then('we should receive error status "{text}"')
def step_impl(context, text):
    assert text == context.status
