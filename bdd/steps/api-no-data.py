import requests
from behave import *


@given('we try to login without data')
def step_impl(context):
    pass

@when('we don\'t provide a username or password')
def step_impl(context):
    res = requests.post('https://reqres.in/api/login', {'email': '', 'password': ''})
    context.status = str(res.status_code)

@then('we should receive error4 status "{text}"')
def step_impl(context, text):
    assert text == context.status
