import requests
from behave import *


@given('we try to login without a username')
def step_impl(context):
    pass

@when('we don\'t provide a username')
def step_impl(context):
    res = requests.post('https://reqres.in/api/login', {'email': '', 'password': 'password123'})
    context.status = str(res.status_code)

@then('we should receive error2 status "{text}"')
def step_impl(context, text):
    assert text == context.status
